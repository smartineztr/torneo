package com.company;


import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner read  = new Scanner(System.in);
        int nteam=1;
        while(nteam %2 !=0) {
            System.out.print("Ingrese numero de equipos en pares: ");
            nteam = read.nextInt();
        }
        Team arrayTeam[] = new Team[nteam];
        Player arrayPlayer[] = new Player[5];
        String name, dt, age, numerodorsal, position;
        for(int i=0; i<nteam; i++) {
            System.out.print("Inserte el nombre del equipo " + (i + 1) + ": ");
            name = read.next();
            System.out.print("Inserte el nombre del DT del " + (i+1) + ": ");
            dt = read.next();
            arrayTeam[i] = new Team(name, dt);
            for (int j = 0; j < 5; j++) {
                System.out.print("Inserte el nombre del jugador " + (j + 1) + ": ");
                name = read.next();
                System.out.print("Inserte la edad del jugador en número " + (j + 1) + ": ");
                age = read.next();
                System.out.print("Inserte el dorsal del jugador en número " + (j + 1) + ": ");
                numerodorsal = read.next();
                System.out.print("Inserte el la posicion del jugador (portero-defensa-ataque) " + (j + 1) + ": ");
                position = read.next();
                arrayPlayer[j] = new Player(name, age, numerodorsal, position);
                System.out.println("Jugador  creado " + "\n" + "Nombre: " + arrayPlayer[j].name + "\n"+ "Edad: "
                        + arrayPlayer[j].age + "\n"+ "Dorsal: " +  arrayPlayer[j].numberdorsal +
                        "\n" + "Posicion: "+ arrayPlayer[j].position);
            }
            System.out.println("Equipo creado " + "Nombre: "+ arrayTeam[i].name + "\n" + "Nombre: "+ arrayTeam[i].dt );
        }
        ArrayList local = new ArrayList();
        ArrayList visitante = new ArrayList();
        for (int j=0;j<arrayTeam.length;j++){
            if (j%2==0){
                local.add(arrayTeam[j].name);
            }
            else {
                visitante.add(arrayTeam[j].name);
            }
        }
        System.out.println("PARTIDOS");
        for (int k=0;k<arrayTeam.length-1;k++) {
            System.out.println(local.get(k) + " VS " + visitante.get(k));
        }
    }
}
